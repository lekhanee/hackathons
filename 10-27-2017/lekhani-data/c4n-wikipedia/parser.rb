olines = []
File.open("freq-ne.txt", "r") do |f|
  f.each_line do |line|
    parts = line.split(",")
    oline = "word=#{parts[1].strip},f=#{parts[0].strip}"
    olines << oline
  end
end
File.open("freqmap.txt", 'w') { |file| file.write("#{olines.join("\n")}") }